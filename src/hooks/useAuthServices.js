import { useState } from 'react';
import axios from 'axios';

const useAuthService = () => {
  const signin = async (username, password) => {
    try {
      const response = await axios.post('http://localhost:3000/api/auth/signin', {
        username,
        password,
      });
      return response;
    } catch (error) {
      throw new Error('An error occurred during signin.');
    }
  };
  const signup = async (username, password, dob) => {
    try {
        const response = await axios.post('http://localhost:3000/api/auth/signup', {

        username,
        password,
        dob,
      });
      return response;
    } catch (error) {
      throw new Error('An error occurred during signin.');
    }
  };

  const getToken = () =>{
    return localStorage.getItem('token')
  }
  
 

  return { signin, signup ,getToken};
};

export default useAuthService;
