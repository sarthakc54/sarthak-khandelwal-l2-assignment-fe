import { useState } from 'react';
import axios from 'axios';
import useAuthService from './useAuthServices'

const useProductService = () => {
  const {getToken} = useAuthService();
  const token =getToken(); 
  const laptopSearch = async (searchTerm, currentPage) => {
    try {
        const response = await axios.get('http://localhost:3000/api/products/laptops/search', {
          headers: {
            Authorization: `Bearer ${token}`, // Add the authorization header
          },
            params: {
              q: searchTerm,
              page: currentPage,
              limit: 10,
            },
          });
    
      return response;
    } catch (error) {
      throw new Error('An error occurred during signin.');
    }
  };
  const allProducts = async (currentPage) => {
    try {
      
        const response = await axios.get('http://localhost:3000/api/products/laptops',{
          headers: {
            Authorization: `Bearer ${token}`, // Add the authorization header
          },   
        params: {
                page: currentPage,
                limit: 10, // Adjust the limit as needed
              },
        }); 
      return response;
    } catch (error) {
      throw new Error('An error occurred during signin.');
    }
  };
 

  return { laptopSearch, allProducts };
};

export default useProductService;
