import React from 'react';
import { Routes as BrowserRoutes,  Route, BrowserRouter } from 'react-router-dom';
import Signup from './pages/Signup';
import Signin from './pages/Signin';
import Home from './pages/Home';

const Routes = () => {
  return (
    <BrowserRouter>
<BrowserRoutes>
      
      <Route exact path="/" element={<Home/>} />
      <Route path="/signup" element={<Signup/>} />
      <Route path="/signin" element={<Signin/>} />

  </BrowserRoutes>
    </BrowserRouter>
    
  );
};

export default Routes;
