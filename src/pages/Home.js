import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { useSelector, useDispatch } from 'react-redux';
import { debounce } from 'lodash';
import { useNavigate } from 'react-router-dom';
import useProductService from '../hooks/useProductService';

const Home = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [data, setData] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(0);
  const navigate = useNavigate();
  const isLogged = useSelector(state=>state.isLogged );
  const {laptopSearch,allProducts} = useProductService();
  //  Checking if user is Logged in? State coming from redux store
  if(!isLogged){

     navigate('/signin')
  }

  // Fetching Data for All products (Laptops)
  useEffect(() => {
    const fetchData = async () => {
      try {
        
        const response = await allProducts(currentPage);
        if(response){
            setData(response.data.laptops);
            setTotalPages(response.data.totalPages);
        }
        
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();
  }, [currentPage]);

  // Seach API call 
  useEffect(()=>{
    const searchApi = debounce(async () => {
        try {
          const response = await laptopSearch(searchTerm,currentPage)
            
          if(response){
            setData(response.data.laptops);
            setTotalPages(response.data.totalPages);
          }
          
        } catch (error) {
          console.error('Error searching data:', error);
        }
      }, 300);

      searchApi();
  },[searchTerm])

  // Render Pagination buttons 
  useEffect(()=>{
      renderPagination();
  },[totalPages])
 
  const handleSearchChange = (e) => {
    const searchTerm = e.target.value;
    setSearchTerm(searchTerm);
  };

  const handlePageChange = (pageNumber) => {
    setCurrentPage(pageNumber);
  };

  const handlePrevPage = () => {
    if (currentPage > 1) {
      setCurrentPage((prevPage) => prevPage - 1);
    }
  };

  const handleNextPage = () => {
    if (currentPage < totalPages) {
      setCurrentPage((prevPage) => prevPage + 1);
    }
  };


  const renderPagination = () =>{
    let pageNumbers = [];
    if (totalPages <= 10) {
        pageNumbers = Array.from({ length: totalPages }, (_, index) => index + 1);
      } else {
        const maxVisiblePages = 10;
        const halfVisiblePages = Math.floor(maxVisiblePages / 2);
        const startPage = Math.max(currentPage - halfVisiblePages, 1);
        const endPage = Math.min(startPage + maxVisiblePages - 1, totalPages);
  
        pageNumbers = Array.from({ length: endPage - startPage + 1 }, (_, index) => startPage + index);
      }
    
      return (
        <>
          {pageNumbers.map((pageNumber) => (
            <button
              key={pageNumber}
              onClick={() => handlePageChange(pageNumber)}
              className={pageNumber === currentPage ? 'active' : ''}
            >
              {pageNumber}
            </button>
          ))}
        </>
      );
 

    }

    // Clear Search
    const handleClearSearch = () =>{
        setSearchTerm('')
    }

  return (
    <div style={{display:'flex', flexDirection:'column', maxWidth:'70%', maxHeight:"80vh"}} className='container'>
      <h2>Home</h2>
      <div className='search-group'>
      <input
        type="text"
        value={searchTerm}
        onChange={handleSearchChange}
        placeholder="Search"
        className='search-input'
      />
        {searchTerm && (
          <button className="clear-search" onClick={handleClearSearch}>
            Clear Search
          </button>
        )}
      </div>
     
<table className="table">
        <thead>
          <tr>
            <th>Name</th>
            <th>Type</th>
            <th>Mfg. date</th>
          </tr>
        </thead>
        <tbody>
          {data?.map((item, index) => (
            <tr key={index}>
              <td>{item.name}</td>
              <td>{item.type}</td>
              <td>{new Date(item.manufacture_date).toUTCString()}</td>
            </tr>
          ))}
        </tbody>
      </table>
      <div className="pagination">
        <button onClick={handlePrevPage} disabled={currentPage === 1}>
          Prev
        </button>
        {renderPagination()}
        <button onClick={handleNextPage} disabled={currentPage === totalPages}>
          Next
        </button>
      </div>
    </div>
  );
};

export default Home;
