import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from "react-router-dom";
import { useSelector,useDispatch } from 'react-redux';
import { signingIn, signingOut } from '../actions';
import useAuthService from '../hooks/useAuthServices';

const Signin = () => {
  const navigate = useNavigate();
  const isLogged = useSelector(state=>state.isLogged );
  if(isLogged){

    navigate('/')
 }

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const dispatch = useDispatch()
  const {signin} = useAuthService();
  const handleUsernameChange = (e) => {
    setUsername(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {


        const response = await signin(username, password)
        console.log('Signin successful!', response.data);
        // Set the login session
      setLoginSession(response.data.token);
      navigate('/')

      } catch (error) {
        console.error('Signin failed:', error);
        setError('Incorrect username or password');
      }
  };
  
  const setLoginSession = (token) => {
    localStorage.setItem('token', token);
  
    const logoutTimeout = setTimeout(handleLogout, 2 * 60 * 1000); // 2 minutes in milliseconds
    setTimeout(()=>refreshTimeout(logoutTimeout, token), 1 * 60 * 1000)
    dispatch(signingIn())
  };

  const refreshTimeout = (logoutTimeout,token) =>{
    if (window.confirm("Your session will expire. Please press ok to continue.")) {
        clearTimeout(logoutTimeout);
        setLoginSession(token)
      } else {
       
      }
  }

  const handleLogout = () => {
    // Clear the JWT token from local storage
    localStorage.removeItem('token');
    setUsername('');
    setPassword('');
    setError('');
    dispatch(signingOut())
 
  };

  return (
    <div className='container'>
      <h2>Signin</h2>
      <form onSubmit={handleSubmit}>
        <div className='form-group'>
          <label>Username:</label>
          <input type="text" value={username} onChange={handleUsernameChange} />
        </div>
        <div className='form-group'>
          <label>Password:</label>
          <input type="password" value={password} onChange={handlePasswordChange} />
        </div>
        {error && <div className="error">{error}</div>}
        <div className='form-group'>
        <button type="submit">Signin</button>
        </div>
      </form>
    </div>
  );
};

export default Signin;
