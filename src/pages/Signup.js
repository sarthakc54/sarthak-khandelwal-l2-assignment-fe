import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import '../auth.css';
import useAuthService from '../hooks/useAuthServices';

const Signup = () => {
  const [username, setUsername] = useState('');
  const [dob, setDob] = useState('');
  const [password, setPassword] = useState('');

  const navigate = useNavigate();
  const {signup} = useAuthService();

  const handleUsernameChange = (e) => {
    setUsername(e.target.value);
  };

  const handleDobChange = (e) => {
    setDob(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await signup(username,password, dob)
        if(response)  navigate('/signin');

    } catch (error) {
      console.error('Signup failed:', error);
    }
  };

  return (
    <>
    <h2>SIGN UP</h2>
    <div className='container'>
      
      <form onSubmit={handleSubmit} className='form'>
        <div className='form-group'>
          <label>Username:</label>
          <input type="text" value={username} onChange={handleUsernameChange} />
        </div>
        <div className='form-group'>
          <label>Date of Birth:</label>
          <input type="date" value={dob} onChange={handleDobChange} style={{width:'10.5rem'}} />
        </div>
        <div className='form-group'>
          <label>Password:</label>
          <input type="password" value={password} onChange={handlePasswordChange} />
        </div>
        <button type="submit">Signup</button>
      </form>
    </div>
    </>
    
  );
};

export default Signup;
