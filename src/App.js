import React from 'react';
import Routes from './Routes';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import allReducer from './reducers';

// Creating Redux Store.
const store = createStore(allReducer)
const App = () => {
  return (
    <Provider store={store}> 
  <div>
      <Routes />
    </div>
    </Provider>
  
  );
};

export default App;
