// ACTIONS for auth

export const signingIn = () =>{
    console.log("SIGNIN ACTION")
    return {
        type:'SIGN_IN'
    }
}

export const signingOut = () =>{
    console.log("SIGNOUT ACTION")
    return {
        type:'SIGN_OUT'
    }
}